[![Screenshot](screenshot.png)](https://ki.avanier.now.sh)

**Ki** is a key map.

---

**By [Avanier](https://avanier.now.sh). Licensed under [MIT](LICENSE).**
